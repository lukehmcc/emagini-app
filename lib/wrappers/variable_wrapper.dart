import 'package:flutter/material.dart';

//  This is a notification wrapper for the selected item list so I don't have to
//use gross globals and so updating stuff updates the taskbar
class VariableWrapper extends ChangeNotifier {
  /// Internal, private state of the list.
  final List<List<int>> indexList = [];

  /// The current length of all items
  int length() {
    return indexList.length;
  }

  // returns the list in case a comparison check is needed
  List<List<int>> getList() {
    return indexList;
  }

  /// Adds [item] to list. This and [removeAll] are the only ways to modify the
  /// cart from the outside.
  void add(List<int> item) {
    indexList.add(item);
    // This call tells the widgets that are listening to this model to rebuild.
    notifyListeners();
  }

  void remove(List<int> item) {
    indexList.remove(item);
    notifyListeners();
  }

  /// Removes all items from the cart.
  void removeAll() {
    indexList.clear();
    // This call tells the widgets that are listening to this model to rebuild.
    notifyListeners();
  }
}
