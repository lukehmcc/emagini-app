import 'package:photo_manager/photo_manager.dart';

class AssetObject {
  // variables it's gonna store
  // these are effectively nulls so I gotta watch for that
  String skylink = "";
  AssetEntity entity = AssetEntity(id: "", typeInt: 0, width: 0, height: 0);
  DateTime creationDate = DateTime.utc(1970, 0, 0);
  String entityBlurHash = "";

  AssetObject(
      this.skylink, this.entity, this.creationDate, this.entityBlurHash);
}
