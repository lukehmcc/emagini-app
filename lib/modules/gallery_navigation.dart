// package imports
import 'package:filesystem_dac/dac.dart';
import 'package:flutter/material.dart';
import 'package:skynet/src/skystandards/fs.dart';
import 'package:get/get.dart';
import 'package:test/screens/video_screen.dart';
import 'package:hive/hive.dart';
import 'package:skynet/src/client.dart';

// file imports
import '../modules/content_indexer.dart';
import '../screens/image_screen.dart';

// This file makes gallery navigation possible

// TODO make this function within the album context

// TODO make this function with videos

// This function is to change the current image showing in the gallery
changeGalleryLocation(
  List<int> index,
  FileSystemDAC dac,
  bool goingRight,
  SkynetClient skynetClient,
  String tempDir,
  Box<dynamic> localFiles,
) async {
  if (goingRight) {
    print("GOING RIGHT");
  } else {
    print("GOING LEFT");
  }
  List<String>? localIndex = await getLocalIndex();
  if (localIndex != null) {
    DirectoryIndex? folderIndex =
        dac.getDirectoryIndexCached(localIndex[index[0]]);
    if (folderIndex != null) {
      if (goingRight) {
        // If going right
        // and it is the last one in the section
        // and there are no futher sections
        if (index[1] == folderIndex.files.entries.length - 1) {
          // and there is another section
          if (index[0] < localIndex.length - 1) {
            // send to first in next section
            _pushItem(
              dac,
              [index[0] + 1, 0],
              localIndex,
              goingRight,
              skynetClient,
              tempDir,
              localFiles,
            );
          } else {
            // nothing happens
          }
        } else {
          // is any other item in the section
          _pushItem(
            dac,
            [index[0], index[1] + 1],
            localIndex,
            goingRight,
            skynetClient,
            tempDir,
            localFiles,
          );
        }
      } else {
        // If going left
        // and it is the first one in the section
        if (index[1] == 0) {
          // and there is another prior section
          if (index[0] > 0) {
            DirectoryIndex? prevFolderIndex =
                dac.getDirectoryIndexCached(localIndex[index[0] - 1]);
            if (prevFolderIndex != null) {
              _pushItem(
                dac,
                [index[0] - 1, prevFolderIndex.files.entries.length - 1],
                localIndex,
                goingRight,
                skynetClient,
                tempDir,
                localFiles,
              );
            }
          } else {
            // nothing happens
          }
        } else {
          // and it is any other one in the section
          _pushItem(
            dac,
            [index[0], index[1] - 1],
            localIndex,
            goingRight,
            skynetClient,
            tempDir,
            localFiles,
          );
        }
      }
    }
  }
}

_pushItem(
  FileSystemDAC dac,
  List<int> index,
  List<String> localIndex,
  bool goingRight,
  SkynetClient skynetClient,
  String tempDir,
  Box<dynamic> localFiles,
) {
  DirectoryIndex? folderIndex =
      dac.getDirectoryIndexCached(localIndex[index[0]]);
  if (folderIndex != null) {
    var fileList = folderIndex.files.entries.toList();
    String thumbnailKey = fileList[index[1]].value.ext?['thumbnail']['key'];
    String bhash = fileList[index[1]].value.ext?['thumbnail']['blurHash'];
    Widget screen;
    if (fileList[index[1]].value.ext?['video'] != null) {
      screen = VideoScreen(
          videoFile: fileList[index[1]].value,
          index: index,
          skynetClient: skynetClient,
          dac: dac,
          tempDir: tempDir,
          localFiles: localFiles,
          bhash: bhash,
          skynetBaseFilePath: localIndex[index[0]]);
    } else {
      screen = ImageScreen(
        index: index,
        bhash: bhash,
        dac: dac,
        thumbKey: thumbnailKey,
        imageFile: fileList[index[1]].value,
        skynetBaseFilePath: localIndex[index[0]],
        skynetClient: skynetClient,
        tempDir: tempDir,
        localFiles: localFiles,
      );
    }

    Transition leftOrRight;
    if (goingRight) {
      leftOrRight = Transition.rightToLeft;
    } else {
      leftOrRight = Transition.leftToRight;
    }
    Get.off(screen,
        transition: leftOrRight, popGesture: true, preventDuplicates: false);
  }
}
