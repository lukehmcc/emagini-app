import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';
import 'dart:ffi';
import 'dart:ui';

import 'package:ffmpeg_kit_flutter_full_gpl/packages.dart';
import 'package:skynet/skynet.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
// This is temporary because the native functions haven't been exported yet
import 'package:skynet/src/mysky_provider/native.dart';
import 'package:filesystem_dac/dac.dart';
import 'package:convert/convert.dart';
import 'package:crypto/crypto.dart';
import 'package:flutter/foundation.dart';
import 'package:filesystem_dac/dac.dart';
import 'package:path/path.dart';
import 'package:skynet/skynet.dart';
import 'package:sodium_libs/sodium_libs.dart';
import 'package:test/modules/ffmpeg.dart';
import 'package:uuid/uuid.dart';
import 'package:skynet/src/encode_endian/encode_endian.dart';
import 'package:skynet/src/encode_endian/base.dart';
import 'package:skynet/src/mysky/encrypted_files.dart';
import 'package:path_provider/path_provider.dart';
import 'package:hive/hive.dart';

const metadataMaxFileSizeNative = 4 * 1000 * 1000;

class StorageService {
  late SkynetClient skynetClient;
  late FileSystemDAC dac;
  late Sodium sodium;
  late String temporaryDirectory;

  Future<List<dynamic>> init() async {
    print("grab application directory");
    Directory appSupDir = await getApplicationSupportDirectory();
    String appSupPath = appSupDir.path;
    print("-------" + appSupPath + "-------");
    print("loading up hive");
    Hive.init(appSupPath);
    dynamic localFiles = await Hive.openBox('localFiles');
    print("grab temp local");
    Directory tempDir = await getTemporaryDirectory();
    temporaryDirectory = tempDir.path;
    print("-------" + temporaryDirectory + "-------");
    print("loading libsodium");
    sodium = await SodiumInit.init();
    // problem for web client
    print("initializing mysky");
    var storage = const FlutterSecureStorage();
    String? userPortal = await storage.read(key: "user-portal");
    String? portalJWT = await storage.read(key: "portal-jwt");
    if (userPortal != null) {
      print("Utilizing user provided portal: $userPortal");
    }
    if (portalJWT != null) {
      print("utilizing portal cookie: $portalJWT");
    }
    skynetClient = SkynetClient(portal: userPortal, cookie: portalJWT);
    final mySkyProvider = NativeMySkyProvider(skynetClient);
    dac = FileSystemDAC(
      mySkyProvider: mySkyProvider,
      skapp: 'emagini.hnsa',
      sodium: sodium,
      debugEnabled: true,
    );
    await dac.init(devEnabled: true);

    print("logging user in");
    String? seedPhraase = await storage.read(key: "user-seed");

    final user = await SkynetUser.fromMySkySeedPhrase(seedPhraase!);
    mySkyProvider.skynetUser = user;
    dac.onUserLogin();
    print("logged in and functional");
    return [dac, skynetClient, temporaryDirectory, localFiles];
  }

  Future<void> uploadOneFile(String path, File file) async {
    String? name;
    try {
      final multihash = await getMultiHashForFile(file);
      final sha1Hash = await getSHA1HashForFile(file);

      name = basename(file.path);

      final ext = extension(file.path).toLowerCase();

      var generateMetadata = metadataSupportedExtensions
              .contains(ext) /* &&
              file.lengthSync() < 20000000 */
          ; // 20 MB

      Map<String, dynamic> additionalExt = {};

      File? videoThumbnailFile;

      // ! Only needed for video metadata
      if (supportedVideoExtensionsForFFmpeg.contains(ext)) {
        try {
          final args = [
            '-v',
            'quiet',
            '-print_format',
            'json',
            '-show_format',
            '-select_streams',
            'v:0',
            '-show_entries',
            'stream=width,height',
            file.path,
          ];

          // then actually get the data
          FFResult dataResult = await runFFProbe(args);
          Map<String, dynamic> data = jsonDecode(dataResult.stdout);
          print("Map Data: " + data.toString());

          final streams = data['streams'];

          final format = data['format'];

          final videoExt = {
            'format_name': format['format_name'],
            'duration': double.tryParse(format['duration']),
            'bit_rate': int.tryParse(format['bit_rate']),
            'streams': streams,
          };

          if ((format['tags'] ?? {}).isNotEmpty) {
            for (final key in format['tags'].keys.toList()) {
              format['tags'][key.toLowerCase()] = format['tags'][key];
            }
            final includedTags = [
              'title',
              'artist',
              'album',
              'album_artist',
              'track',
              'date',
              'comment',
            ];
            for (final tag in includedTags) {
              if (format['tags'][tag] != null) {
                videoExt[tag] = format['tags'][tag];
              }
            }
          }

          additionalExt['video'] = videoExt;

          final outFile = File(join(
            temporaryDirectory,
            '$multihash-thumbnail-extract.jpg',
          ));
          if (!outFile.existsSync()) {
            final extractThumbnailArgs = [
              '-i',
              file.path,
              '-ss',
              '00:00:01.000',
              '-vframes',
              '1',
              outFile.path,
            ];
            print("running ffmpeg on following args: " + args.toString());
            FFResult ffmpegResult = await runFFMpeg(extractThumbnailArgs);
            print("FFmpeg results: " + ffmpegResult.stdout);
            print("FFmpeg exit code: " + ffmpegResult.exitCode.toString());
          }

          if (outFile.existsSync()) {
            print("Grabbing thumbnail worked!");
            videoThumbnailFile = outFile;
            generateMetadata = true;
          } else {
            print("thumbnail file does not exist!!!!");
          }
        } catch (e) {
          // TODO Handle error
        }
      }

      final fileData = await dac.uploadFileData(
        multihash,
        file.lengthSync(),
        customEncryptAndUploadFileFunction: () async {
          return await encryptAndUploadFileInChunks(file, multihash);
        },
        generateMetadata: generateMetadata,
        filename: file.path,
        additionalExt: additionalExt,
        hashes: [sha1Hash],
        generateMetadataWrapper: (
          extension,
          rootPathSeed,
        ) async {
          final metadataSize = supportedImageExtensions.contains(ext)
              ? file.lengthSync()
              : min(
                  metadataMaxFileSizeNative,
                  file.lengthSync(),
                );

          final res = await compute(extractMetadata, [
            videoThumbnailFile == null ? extension : 'video-thumbnail',
            videoThumbnailFile == null
                ? Uint8List.fromList(await file
                    .openRead(
                    0,
                    metadataSize,
                  )
                    .fold<List<int>>(
                        <int>[], (previous, element) => previous + element))
                : await videoThumbnailFile.readAsBytes(),
            rootPathSeed,
          ]);
          return res;
        },
      );

      final res = await dac.createFile(
        path,
        name,
        fileData,
      );
      if (!res.success) {
        throw res.error!;
      }
    } catch (e, st) {
      // TODO Handle the error
    }
  }

  Future<String> getMultiHashForFile(File file) async {
    if (Platform.isLinux) {
      final res = await Process.run('sha256sum', [file.path]);
      final String hash = res.stdout.split(' ').first;
      if (hash.length != 64) {
        throw 'Hash function failed';
      }
      return '1220$hash';
    }
    var output = AccumulatorSink<Digest>();
    var input = sha256.startChunkedConversion(output);
    await file.openRead().forEach(input.add);
    input.close();
    final hash = output.events.single;
    return '1220$hash';
  }

  Future<String> getSHA1HashForFile(File file) async {
    if (Platform.isLinux) {
      final res = await Process.run('sha1sum', [file.path]);
      final String hash = res.stdout.split(' ').first;
      if (hash.length != 40) {
        throw 'Hash function failed';
      }
      return '1114$hash';
    }
    var output = AccumulatorSink<Digest>();
    var input = sha1.startChunkedConversion(output);
    await file.openRead().forEach(input.add);
    input.close();
    final hash = output.events.single;
    return '1114$hash';
  }

  Future<EncryptAndUploadResponse> encryptAndUploadFileInChunks(
    File file,
    String fileMultiHash,
  ) async {
    int padding = 0;
    const maxChunkSize = 1 * 1024 * 1024; // 1 MB

    final fileStateNotifier = dac.getFileStateChangeNotifier(fileMultiHash);

    fileStateNotifier.updateFileState(FileState(
      type: FileStateType.encrypting,
      progress: 0,
    ));

    final outFile = File(join(
      temporaryDirectory,
      'encrypted_files',
      Uuid().v4(),
    ));

    outFile.createSync(recursive: true);

    final totalSize = file.lengthSync();

    final secretKey = dac.sodium.crypto.secretBox.keygen();

    int internalSize = 0;
    int currentSize = 0;

    final sink = outFile.openWrite();

    final streamCtrl = StreamController<PlaintextChunk>();

    final List<int> data = [];

    file.openRead().listen((event) {
      data.addAll(event);

      internalSize += event.length;

      while (data.length >= (maxChunkSize)) {
        streamCtrl.add(
          PlaintextChunk(
            Uint8List.fromList(
              data.sublist(0, maxChunkSize),
            ),
            false,
          ),
        );
        data.removeRange(0, maxChunkSize);
      }
      if (internalSize == totalSize) {
        streamCtrl.add(PlaintextChunk(
          Uint8List.fromList(
            data,
          ),
          true,
        ));
        streamCtrl.close();
      }
    });
    final completer = Completer<bool>();

    int i = 0;

    await for (var chunk in streamCtrl.stream) {
      final nonce = Uint8List.fromList(
        encodeEndian(i, dac.sodium.crypto.secretBox.nonceBytes,
            endianType: EndianType.littleEndian) as List<int>,
      );

      i++;

      if (chunk.isLast) {
        padding = padFileSize(totalSize) - totalSize;
        if ((padding + chunk.bytes.length) >= maxChunkSize) {
          padding = maxChunkSize - chunk.bytes.length;
        }

        final bytes = Uint8List.fromList(
          chunk.bytes +
              Uint8List(
                padding,
              ),
        );
        chunk = PlaintextChunk(bytes, true);
      }

      final res = dac.sodium.crypto.secretBox.easy(
        message: chunk.bytes,
        nonce: nonce,
        key: secretKey,
      );

      currentSize += chunk.bytes.length;
      fileStateNotifier.updateFileState(
        FileState(
          type: FileStateType.encrypting,
          progress: currentSize / totalSize,
        ),
      );
      sink.add(res);
      if (currentSize >= totalSize) {
        completer.complete(true);
      }
    }

    await completer.future;

    await sink.close();

    fileStateNotifier.updateFileState(
      FileState(
        type: FileStateType.encrypting,
        progress: 1,
      ),
    );

    fileStateNotifier.updateFileState(
      FileState(
        type: FileStateType.uploading,
        progress:
            0, // TODO Why is the upload speed slowed down when setting this to null?!?!?!?!
      ),
    );

    String? skylink;

    final TUS_CHUNK_SIZE = (1 << 22) * 10; // ~ 41 MB

    if (outFile.lengthSync() > TUS_CHUNK_SIZE) {
      skylink = await skynetClient.upload.uploadLargeFile(
        XFileDart(outFile.path),
        filename: 'fs-dac.hns',
        fingerprint: Uuid().v4(),
        onProgress: (value) {
          fileStateNotifier.updateFileState(
            FileState(
              type: FileStateType.uploading,
              progress: value,
            ),
          );
        },
      );
    } else {
      skylink = await skynetClient.upload.uploadFileWithStream(
        SkyFile(
          content: Uint8List(0),
          filename: 'fs-dac.hns',
          type: 'application/octet-stream',
        ),
        outFile.lengthSync(),
        outFile.openRead().map((event) => Uint8List.fromList(event)),
        onProgress: (value) {
          fileStateNotifier.updateFileState(
            FileState(
              type: FileStateType.uploading,
              progress: value,
            ),
          );
        },
      );
    }

    await outFile.delete();

    if (skylink == null) {
      throw 'File Upload failed';
    }
    fileStateNotifier.updateFileState(
      FileState(
        type: FileStateType.idle,
        progress: null,
      ),
    );

    return EncryptAndUploadResponse(
      skylink: skylink,
      secretKey: secretKey.extractBytes(),
      encryptionType: 'libsodium_secretbox',
      maxChunkSize: maxChunkSize,
      padding: padding,
    );
  }
}

class PlaintextChunk {
  final Uint8List bytes;
  final bool isLast;
  PlaintextChunk(this.bytes, this.isLast);
}
