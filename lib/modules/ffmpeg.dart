// package imports
import 'dart:io';

import 'package:ffmpeg_kit_flutter_full_gpl/ffmpeg_kit.dart';
import 'package:ffmpeg_kit_flutter_full_gpl/ffprobe_kit.dart';
import 'package:universal_platform/universal_platform.dart';

// file imports

// This is code taken from Redsolver's Vup: (https://github.com/redsolver/vup)

class FFResult {
  final int exitCode;
  final String stdout;

  FFResult({
    required this.exitCode,
    required this.stdout,
  });
  toString() => 'FFResult{$exitCode, $stdout}';
}

String get ffmpegPath => 'ffmpeg';
String get ffprobePath => 'ffprobe';

bool _useLibrary = !(UniversalPlatform.isLinux || UniversalPlatform.isWindows);

Future<FFResult> runFFProbe(List<String> args) async {
  if (_useLibrary) {
    final session = await FFprobeKit.executeWithArgumentsAsync(args);

    int? exitCode;
    while (exitCode == null) {
      final returnCode = await session.getReturnCode();
      exitCode = returnCode?.getValue();
      await Future.delayed(const Duration(milliseconds: 10));
    }

    final output = await session.getOutput();
    return FFResult(exitCode: exitCode, stdout: output!);
  } else {
    final res = await Process.run(ffprobePath, args);
    return FFResult(
      exitCode: res.exitCode,
      stdout: res.stdout,
    );
  }
}

Future<FFResult> runFFMpeg(List<String> args) async {
  if (_useLibrary) {
    final session = await FFmpegKit.executeWithArgumentsAsync(args);

    int? exitCode;
    while (exitCode == null) {
      final returnCode = await session.getReturnCode();
      exitCode = returnCode?.getValue();
      await Future.delayed(const Duration(milliseconds: 10));
    }

    final output = await session.getOutput();
    return FFResult(exitCode: exitCode, stdout: output!);
  } else {
    final res = await Process.run(ffmpegPath, args);
    return FFResult(
      exitCode: res.exitCode,
      stdout: res.stdout,
    );
  }
}
