// package imports
import 'package:path/path.dart' as pathie;

// a couple functions to make comparing date strings easier

String dateToHumanReadable(String folderPath) {
  List<String> dateSplits = pathie.split(folderPath);
  return monthIntToString(int.parse(dateSplits[2])) +
      " " +
      dateSplits[3] +
      ", " +
      dateSplits[1];
}

String monthIntToString(int monthInt) {
  String monthString = "";
  if (monthInt == 1) {
    monthString = "January";
  } else if (monthInt == 2) {
    monthString = "Febuary";
  } else if (monthInt == 3) {
    monthString = "March";
  } else if (monthInt == 4) {
    monthString = "April";
  } else if (monthInt == 5) {
    monthString = "May";
  } else if (monthInt == 6) {
    monthString = "June";
  } else if (monthInt == 7) {
    monthString = "July";
  } else if (monthInt == 8) {
    monthString = "August";
  } else if (monthInt == 9) {
    monthString = "September";
  } else if (monthInt == 10) {
    monthString = "October";
  } else if (monthInt == 11) {
    monthString = "November";
  } else {
    monthString = "December";
  }
  return monthString;
}
