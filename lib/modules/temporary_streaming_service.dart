import 'dart:io';
import 'dart:math';

// package imports
import 'package:alfred/alfred.dart';
import 'package:intl/intl.dart';
import 'package:network_info_plus/network_info_plus.dart';
import 'package:random_string/random_string.dart';
import 'package:skynet/src/skystandards/fs.dart';
import 'package:skynet/src/client.dart';
import 'package:hive/hive.dart' as h;
import 'package:filesystem_dac/dac.dart';

// import 'package:vup/generic/state.dart';
// import 'package:vup/service/base.dart';
// import 'package:vup/service/web_server/serve_chunked_file.dart';

// file imports
import '../modules/storage_provider.dart';
import '../modules/serve_chunked_file.dart';

// This is a modified version of the temporary streaming servicec from
// Redsolver's vup (https://github.com/redsolver/vup)

class TemporaryStreamingServerService {
  bool isRunning = false;
  late Alfred app;
  late int totalSize;
  late SkynetClient skynetClient;
  late FileSystemDAC dac;
  late String tempDir;
  late h.Box<dynamic> localFiles;

  void stop() {
    print('stopping server...');
    app.close(force: true);
    isRunning = false;
    print('stopped server.');
  }

  final availableFiles = <String, DirectoryFile>{};

  Future<String> makeFileAvailable(
      DirectoryFile file,
      SkynetClient localSkynetClient,
      FileSystemDAC localdac,
      String localTempDir,
      h.Box<dynamic> localLocalFiles) async {
    skynetClient = localSkynetClient;
    dac = localdac;
    localFiles = localLocalFiles;
    tempDir = localTempDir;
    totalSize = file.file.size;
    start(43913, '0.0.0.0');
    final streamingKey = randomAlphaNumeric(
      32,
      provider: CoreRandomProvider.from(
        Random.secure(),
      ),
    ).toLowerCase();
    availableFiles[streamingKey] = file;

    final info = NetworkInfo();
    String? ipAddress;
    try {
      ipAddress = await info.getWifiIP();
    } catch (_) {}
    ipAddress ??= '127.0.0.1';

    return 'http://$ipAddress:43913/stream/$streamingKey/${file.name}';
  }

  void start(int port, String bindIp) {
    if (isRunning) return;
    isRunning = true;

    print('starting server...');

    app = Alfred();

    Map<String, String> getHeadersForFile(DirectoryFile file) {
      final df = DateFormat('EEE, dd MMM yyyy HH:mm:ss');
      final dt = DateTime.fromMillisecondsSinceEpoch(file.modified).toUtc();
      return {
        'Accept-Ranges': 'bytes',
        'Content-Length': file.file.size.toString(),
        'Content-Type': file.mimeType ?? 'application/octet-stream',
        'Etag': '"${file.file.hash}"',
        'Last-Modified': df.format(dt) + ' GMT',
      };
    }

    app.get('/stream/:streamingKey/:filename', (req, res) async {
      final key = req.params['streamingKey'];

      final file = availableFiles[key];
      if (file == null) {
        res.statusCode = HttpStatus.notFound;
        return '';
      }

      for (final e in getHeadersForFile(file).entries) {
        res.headers.set(e.key, e.value);
      }

      if (file.file.encryptionType == 'libsodium_secretbox') {
        await handleChunkedFile(
          req,
          res,
          file,
          skynetClient,
          dac,
          tempDir,
          totalSize,
          localFiles,
        );
        return null;
      }
    });

    app.head('/stream/:streamingKey/:filename', (req, res) async {
      final key = req.params['streamingKey'];

      final file = availableFiles[key];
      if (file == null) {
        res.statusCode = HttpStatus.notFound;
        return null;
      }

      for (final e in getHeadersForFile(file).entries) {
        res.headers.set(e.key, e.value);
      }

      return '';
    });

    print('server is running at $bindIp:$port');

    app.listen(port, bindIp);
  }
}
