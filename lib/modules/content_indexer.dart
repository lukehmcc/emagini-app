// package imports
import 'package:skynet/src/skystandards/fs.dart';
import 'package:filesystem_dac/dac.dart';
import 'package:photo_manager/photo_manager.dart';
import 'dart:io';
import 'package:path/path.dart' as pathie;
import 'package:path_provider/path_provider.dart';
import 'package:test/modules/local_files.dart';
import 'package:test/modules/storage_provider.dart';
import 'dart:convert';

// file imports
import '../modules/storage_provider.dart';

// this class indexes the files already uploaded to SkyFS and then writes
// the index to disk
indexSkynetFiles(FileSystemDAC dac, String? lastUploadedPath) async {
  // I'm gonna create a new index every time to make sure everything is up to
  // date, in the future this will have to be optimized
  List<String> paths = [];
  try {
    // this try block is here because there is a good chance that this will
    // fail because some chucklenut puts a non-int string in their emagini
    // directory
    DirectoryIndex? yearIndex = dac.getDirectoryIndexCached("emagini.hnsa");
    if (yearIndex != null) {
      List<String> yearListString = yearIndex.directories.keys.toList();
      List<int> yearListInt = yearListString.map(int.parse).toList();
      yearListInt.sort();
      for (var yearPath in yearListInt.reversed) {
        DirectoryIndex? monthIndex =
            dac.getDirectoryIndexCached("emagini.hnsa/" + yearPath.toString());
        if (monthIndex != null) {
          List<String> monthListString = monthIndex.directories.keys.toList();
          List<int> monthListInt = monthListString.map(int.parse).toList();
          monthListInt.sort();
          for (var monthPath in monthListInt.reversed) {
            DirectoryIndex? dayIndex = dac.getDirectoryIndexCached(
                "emagini.hnsa/" +
                    yearPath.toString() +
                    "/" +
                    monthPath.toString());
            if (dayIndex != null) {
              List<String> dayListString = dayIndex.directories.keys.toList();
              List<int> dayListInt = dayListString.map(int.parse).toList();
              dayListInt.sort();
              for (var dayPath in dayListInt.reversed) {
                paths.add("emagini.hnsa/" +
                    yearPath.toString() +
                    "/" +
                    monthPath.toString() +
                    "/" +
                    dayPath.toString());
                print("added: " + paths.last);
              }
            }
          }
        }
      }
    }
    // then if it isn't empty, save the file
    if (paths != []) {
      setLocalIndex(paths);
      // TODO persist this to skynet
    }
  } catch (e) {
    print(e);
  }
}

// fetch all photos then upload them one by one and send them in for indexing
indexFilesAndUploadSkynet(
    FileSystemDAC dac, StorageService storageService) async {
  print("fetching assets");
  var assets = await _fetchAssets();
  for (var asset in assets) {
    print("uploading file");
    File? file = await asset.file;
    if (file == null) {
      continue;
    }
    // build the date YYYY.MM.DD
    DateTime date = asset.createDateTime;
    String day = date.day.toString();
    String month = date.month.toString();
    String year = date.year.toString();
    // also create the directories just in case
    var taskResult = await dac.createDirectory("emagini.hnsa", year);
    print(taskResult.toJson());
    taskResult = await dac.createDirectory("emagini.hnsa/" + year, month);
    print(taskResult.toJson());
    taskResult =
        await dac.createDirectory("emagini.hnsa/" + year + "/" + month, day);
    print(taskResult.toJson());
    await storageService.uploadOneFile(
        "emagini.hnsa/" + year + "/" + month + "/" + day, file);
    String fullPath = "emagini.hnsa/" +
        year +
        "/" +
        month +
        "/" +
        day +
        "/" +
        pathie.basename(file.path);
    setLocalFile(fullPath, file.absolute.path); // saves the local file path
    await indexSkynetFiles(dac, fullPath);
  }
}

// This fetches all local assets
Future<List<AssetEntity>> _fetchAssets() async {
  // Set onlyAll to true, to fetch only the 'Recent' album
  // which contains all the photos/videos in the storage
  final albums = await PhotoManager.getAssetPathList(onlyAll: true);
  final recentAlbum = albums.first;
  // Now that we got the album, fetch all the assets it contains
  final recentAssets = await recentAlbum.getAssetListRange(
    start: 0, // start at index 0
    end: 100000000, // end at a very big index (to get all the assets)
  );
  // Update the state and notify UI
  return recentAssets;
}

// gets the index
Future<List<String>> getLocalIndex() async {
  Directory appSupDir = await getApplicationSupportDirectory();
  String indexPath = pathie.join(appSupDir.path, "index.txt");
  File indexFile = File(indexPath);
  if (indexFile.existsSync()) {
    List<dynamic> indexDynam = jsonDecode(indexFile.readAsStringSync());
    if (indexDynam == []) {
      return [];
    }
    List<String> indexString = [];
    for (var o in indexDynam) {
      if (o.runtimeType == String) {
        indexString.add(o);
      }
    }
    return indexString;
  } else {
    return [];
  }
}

// sets the index
setLocalIndex(List<String> indexToWrite) async {
  Directory appSupDir = await getApplicationSupportDirectory();
  String indexPath = pathie.join(appSupDir.path, "index.txt");
  File indexFile = File(indexPath);
  indexFile.writeAsStringSync(jsonEncode(indexToWrite));
}
