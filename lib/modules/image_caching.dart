// package imports
import 'dart:convert';
import 'dart:typed_data';
import 'package:crypto/crypto.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'package:path/path.dart' as pathie;

// file imports

// This is a function for dealing with caching of files
Future<String> cacheImage(Uint8List bytes, String skynetFilePath) async {
  // first get the key
  String imageKey = "emagini-" + sha256.convert(bytes).toString();
  Directory appSupDir = await getApplicationSupportDirectory();
  String cachePath = pathie.join(appSupDir.path, "cache");
  // now create the file path
  String localFilePath = pathie.join(cachePath, imageKey);
  File localFile = await File(localFilePath).create(recursive: true);
  // write the file
  await localFile.writeAsBytes(bytes, flush: true);
  // then add it to the cache file and write back to disk
  Map<String, dynamic> cacheMap = await _getCacheMap();
  cacheMap[skynetFilePath] = imageKey;
  await setCacheMap(cacheMap);
  return imageKey;
}

// checks for the existance of locally cached image
Future<bool> checkForCachedImage(String key) async {
  Directory appSupDir = await getApplicationSupportDirectory();
  String cachePath = pathie.join(appSupDir.path, "cache.json");
  File cacheFile = File(cachePath);
  if (cacheFile.existsSync() != true) {
    return false;
  }
  Map<String, dynamic> cacheMap = jsonDecode(cacheFile.readAsStringSync());
  if (cacheMap[key] != null) {
    return true;
  } else {
    return false;
  }
}

// self explain
Future<Uint8List> getCachedImage(String key) async {
  Directory appSupDir = await getApplicationSupportDirectory();
  String cachePath = pathie.join(appSupDir.path, "cache.json");
  File cacheFile = File(cachePath);
  Map<String, dynamic> cacheMap = jsonDecode(cacheFile.readAsStringSync());
  String imagePath = pathie.join(appSupDir.path, "cache", cacheMap[key]);
  File imageFile = File(imagePath);
  return imageFile.readAsBytesSync();
}

// This is a simple function to check the existance of a local cache file
// and create it if it doesn't exist
Future<Map<String, dynamic>> _getCacheMap() async {
  Directory appSupDir = await getApplicationSupportDirectory();
  String cachePath = pathie.join(appSupDir.path, "cache.json");
  File cacheFile = File(cachePath);
  if (cacheFile.existsSync()) {
    Map<String, dynamic> cacheMap = jsonDecode(cacheFile.readAsStringSync());
    return cacheMap;
  } else {
    cacheFile.writeAsStringSync(jsonEncode({}));
    return {};
  }
}

Future<void> setCacheMap(Map<String, dynamic> map) async {
  Directory appSupDir = await getApplicationSupportDirectory();
  String cachePath = pathie.join(appSupDir.path, "cache.json");
  File cacheFile = File(cachePath);
  cacheFile.writeAsStringSync(jsonEncode(map));
}

Future<void> clearCache() async {
  Directory appSupDir = await getApplicationSupportDirectory();
  String cachePath = pathie.join(appSupDir.path, "cache.json");
  File cacheFile = File(cachePath);
  cacheFile.writeAsStringSync(jsonEncode({}));
}
