// package imports
import 'dart:convert';
import 'dart:typed_data';
import 'package:crypto/crypto.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'package:path/path.dart' as pathie;

// file imports

// This is a set of function for dealing with local copies of files

Future<bool> checkForLocalImage(String skynetFilePath) async {
  Map<String, dynamic> localFileMap = await _getLocalFileMap();
  if (localFileMap[skynetFilePath] != null &&
      File(localFileMap[skynetFilePath]).existsSync()) {
    return true;
  } else {
    return false;
  }
}

Future<File?> getLocalFile(String skynetPath) async {
  Map<String, dynamic> localFileMap = await _getLocalFileMap();
  String absPath = localFileMap[skynetPath];
  if (absPath.runtimeType == String) {
    return File(absPath);
  }
  return null;
}

setLocalFile(String skynetPath, String absPath) async {
  Map<String, dynamic> localFileMap = await _getLocalFileMap();
  localFileMap.addAll({skynetPath: absPath});
  _setLocalFileMap(localFileMap);
}

Future<Map<String, dynamic>> _getLocalFileMap() async {
  Directory appSupDir = await getApplicationSupportDirectory();
  String cachePath = pathie.join(appSupDir.path, "local_files.json");
  File cacheFile = File(cachePath);
  if (cacheFile.existsSync()) {
    Map<String, dynamic> cacheMap = jsonDecode(cacheFile.readAsStringSync());
    return cacheMap;
  } else {
    cacheFile.writeAsStringSync(jsonEncode({}));
    return {};
  }
}

_setLocalFileMap(Map<String, dynamic> mapToBeSet) async {
  Directory appSupDir = await getApplicationSupportDirectory();
  String cachePath = pathie.join(appSupDir.path, "local_files.json");
  File cacheFile = File(cachePath);
  cacheFile.writeAsStringSync(jsonEncode(mapToBeSet));
}
