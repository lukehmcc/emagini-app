// package imports
import 'package:flutter/material.dart';
import 'package:get/get.dart';

// file imports
import '../screens/general_settings.dart';

Widget defaultDrawer() {
  return Drawer(
    child: Column(
      // Important: Remove any padding from the ListView.
      children: [
        const Spacer(),
        GestureDetector(
          onTap: () {
            Get.to(const GeneralSettingsScreen());
          },
          child: Container(
              padding: const EdgeInsets.all(16.0),
              child: Wrap(
                crossAxisAlignment: WrapCrossAlignment.center,
                children: const [
                  Text('Settings'),
                  Icon(Icons.settings),
                ],
              )),
        )
      ],
    ),
  );
}
