// package imports
import 'package:flutter/material.dart';
import 'package:test/widgets/thumbnail.dart';
import 'package:hive/hive.dart';
import 'package:skynet/src/client.dart';
import 'package:skynet/src/skystandards/fs.dart';

// file imports
import '../widgets/date_header.dart';
import '../modules/date_strings.dart';
import 'package:filesystem_dac/dac.dart';

// This represents a section of the gallery on a specific date

class GallerySection extends StatefulWidget {
  const GallerySection({
    Key? key,
    required this.assetPath,
    required this.index,
    required this.dac,
    required this.tempDir,
    required this.localFiles,
    required this.skynetClient,
  }) : super(key: key);

  final String assetPath;
  final int index;
  final FileSystemDAC dac;
  final SkynetClient skynetClient;
  final String tempDir;
  final Box<dynamic> localFiles;

  @override
  _GallerySectionState createState() => _GallerySectionState();
}

class _GallerySectionState extends State<GallerySection> {
  Widget gridSection = Container();
  // This section builds in two steps, first with the cached directory index and
  // then with the networked one (if available)
  _gridBuilder(String assetPath, double constraint) async {
    // step one
    DirectoryIndex? dirIndex = widget.dac.getDirectoryIndexCached(assetPath);
    List<Widget> rows = [
      Row(
        children: [
          DateHeader(title: dateToHumanReadable(widget.assetPath)),
        ],
      )
    ];
    int rowLength = (constraint / 120).round();
    if (dirIndex != null) {
      _sectionCompiler(dirIndex, rowLength, rows);
    } else {
      rows.add(Row(
        children: [Container()],
      ));
    }
    // step two
    widget.dac.getDirectoryIndex(assetPath).then((dirIndexNetwork) {
      if (dirIndexNetwork.files.isNotEmpty) {
        _sectionCompiler(dirIndexNetwork, rowLength, rows);
      } else {
        rows.add(Row(
          children: [Container()],
        ));
      }
    });
  }

  _sectionCompiler(DirectoryIndex dirIndex, int rowLength, List<Widget> rows) {
    for (var i = 0; i < dirIndex.files.keys.length; i += rowLength) {
      List<Widget> tempList = [];
      for (var c = 0; c < rowLength; c++) {
        if (dirIndex.files.keys.length <= i + c) {
          // and if the index does exist, then just add the asset normally
          tempList.add(Expanded(
            child: Container(),
          ));
        } else {
          // and if the index does exist, then just add the asset normally
          var fileList = dirIndex.files.entries.toList();
          if (fileList[i + c].value.ext?['thumbnail'] == null) {
            continue;
          }
          String thumbnailKey = fileList[i + c].value.ext?['thumbnail']['key'];
          String bhash = fileList[i + c].value.ext?['thumbnail']['blurHash'];
          tempList.add(Expanded(
            child: AssetThumbnail(
              index: [widget.index, i + c],
              asset: fileList[i + c].value,
              thumbKey: thumbnailKey,
              bhash: bhash,
              dac: widget.dac,
              filePath: widget.assetPath,
              tempDir: widget.tempDir,
              localFiles: widget.localFiles,
              skynetClient: widget.skynetClient,
            ),
          ));
        }
      }
      rows.add(Row(
        children: tempList,
      ));
    }
    setState(() {
      gridSection = Column(
        children: rows,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        _gridBuilder(widget.assetPath, constraints.maxWidth);
        return gridSection;
      },
    );
  }
}
