// package imports
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

// file imports
import '../wrappers/variable_wrapper.dart';

class GalleryAppBar extends StatefulWidget implements PreferredSizeWidget {
  const GalleryAppBar({
    Key? key,
    required this.resetState,
  })  : preferredSize = const Size.fromHeight(kToolbarHeight),
        super(key: key);

  final Function resetState;

  @override
  final Size preferredSize; // default is 56.0

  @override
  _GalleryAppBarState createState() => _GalleryAppBarState();
}

class _GalleryAppBarState extends State<GalleryAppBar> {
  @override
  Widget build(BuildContext context) {
    return Consumer<VariableWrapper>(
      builder: (context, cart, child) {
        if (Provider.of<VariableWrapper>(context, listen: false).length() !=
            0) {
          return AppBar(
            elevation: 4,
            centerTitle: true,
            backgroundColor: Colors.green,
            actions: <Widget>[
              Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: GestureDetector(
                    onTap: () {
                      Provider.of<VariableWrapper>(context, listen: false)
                          .removeAll();
                      widget.resetState();
                    },
                    child: const Icon(
                      Icons.close,
                      size: 26.0,
                    ),
                  )),
              Expanded(child: Container()),
              Padding(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: GestureDetector(
                    onTap: () {
                      const snackBar = SnackBar(
                        content: Text('Deleted Selection'),
                      );
                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                    },
                    child: const Icon(
                      Icons.delete,
                      size: 26.0,
                    ),
                  )),
            ],
          );
        }
        return AppBar(
          elevation: 4,
          centerTitle: true,
          backgroundColor: Colors.green,
          actions: const <Widget>[],
        );
      },
    );
  }
}
