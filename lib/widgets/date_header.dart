import 'package:flutter/material.dart';

// This represents a section of the gallery on a specific date

class DateHeader extends StatefulWidget {
  const DateHeader({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _DateHeaderState createState() => _DateHeaderState();
}

class _DateHeaderState extends State<DateHeader> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10),
      alignment: Alignment.center,
      child: Text(widget.title),
    );
  }
}
