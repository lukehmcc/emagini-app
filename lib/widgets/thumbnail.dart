// package imports
import 'package:filesystem_dac/dac.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:photo_manager/photo_manager.dart';
import 'package:provider/provider.dart';
import 'package:blurhash_dart/blurhash_dart.dart';
import 'package:image/image.dart' as image_2;
import 'dart:typed_data';
import 'package:skynet/src/skystandards/fs.dart';
import 'package:skynet/src/client.dart';
import 'package:hive/hive.dart';

// file imports
import '../wrappers/variable_wrapper.dart';
import '../screens/video_screen.dart';
import '../screens/image_screen.dart';

class AssetThumbnail extends StatefulWidget {
  const AssetThumbnail({
    Key? key,
    required this.asset,
    required this.bhash,
    required this.index,
    required this.thumbKey,
    required this.dac,
    required this.filePath,
    required this.tempDir,
    required this.localFiles,
    required this.skynetClient,
  }) : super(key: key);

  final DirectoryFile asset;
  final String bhash;
  final List<int> index;
  final String thumbKey;
  final FileSystemDAC dac;
  final String filePath;
  final SkynetClient skynetClient;
  final String tempDir;
  final Box<dynamic> localFiles;

  @override
  State<AssetThumbnail> createState() => _AssetThumbnailState();
}

class _AssetThumbnailState extends State<AssetThumbnail> {
  double paddingAmnt = 0;

  @override
  Widget build(BuildContext context) {
    // gotta get the thumbnail
    if (widget.thumbKey == "") {
      Container();
    }
    return FutureBuilder<dynamic>(
      future: widget.dac.loadThumbnail(widget.thumbKey),
      // function where you call your api
      builder: (_, snapshot) {
        final bytes = snapshot.data;
        // If we have no data, display a spinner
        if (bytes == null) {
          if (widget.bhash != "") {
            var blurHash = BlurHash.decode(widget.bhash).toImage(100, 100);
            return Image.memory(
                Uint8List.fromList(image_2.encodeJpg(blurHash)));
          } else {
            return const Center(child: CircularProgressIndicator());
          }
        }
        if (Provider.of<VariableWrapper>(context, listen: false)
            .getList()
            .contains(widget.index)) {
          paddingAmnt = 10;
        } else {
          paddingAmnt = 0;
        }
        // If there's data, display it as an image
        return Padding(
            padding: EdgeInsets.all(paddingAmnt),
            child: InkWell(
              onTap: () {
                // if it's select mode then delselect on press
                if (Provider.of<VariableWrapper>(context, listen: false)
                        .length() !=
                    0) {
                  if (Provider.of<VariableWrapper>(context, listen: false)
                          .getList()
                          .contains(widget.index) ==
                      false) {
                    Provider.of<VariableWrapper>(context, listen: false)
                        .add(widget.index);
                    setState(() {}); // this force redraws the widget
                    print(Provider.of<VariableWrapper>(context, listen: false)
                        .getList());
                  } else {
                    Provider.of<VariableWrapper>(context, listen: false)
                        .remove(widget.index);
                    setState(() {}); // this force redraws the widget
                    print(Provider.of<VariableWrapper>(context, listen: false)
                        .getList());
                    if (Provider.of<VariableWrapper>(context, listen: false)
                            .length() ==
                        0) {
                      // change appbar on selection reset
                    }
                  }
                  // if not then go to the photo or video player
                } else {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) {
                        if (widget.asset.ext?['video'] == null) {
                          // If this is an image, navigate to ImageScreen
                          return ImageScreen(
                            index: widget.index,
                            bhash: widget.bhash,
                            dac: widget.dac,
                            thumbKey: widget.thumbKey,
                            imageFile: widget.asset,
                            skynetBaseFilePath: widget.filePath,
                            localFiles: widget.localFiles,
                            skynetClient: widget.skynetClient,
                            tempDir: widget.tempDir,
                          );
                          // return CircularProgressIndicator();
                        } else {
                          // if it's not, navigate to VideoScreen
                          return VideoScreen(
                            bhash: widget.bhash,
                            index: widget.index,
                            dac: widget.dac,
                            videoFile: widget.asset,
                            skynetBaseFilePath: widget.filePath,
                            localFiles: widget.localFiles,
                            skynetClient: widget.skynetClient,
                            tempDir: widget.tempDir,
                          );
                        }
                      },
                    ),
                  );
                }
              },
              onLongPress: () {
                // make sure to only set to select mode once to avoid weirdness
                if (Provider.of<VariableWrapper>(context, listen: false)
                        .getList()
                        .contains(widget.index) ==
                    false) {
                  if (Provider.of<VariableWrapper>(context, listen: false)
                          .length() ==
                      0) {
                    // do something on selection reset
                  }
                  Provider.of<VariableWrapper>(context, listen: false)
                      .add(widget.index);
                  setState(() {}); // reloads the thumbnail
                  print(Provider.of<VariableWrapper>(context, listen: false)
                      .getList());
                  // now if it's going into select mode then add the header
                } else {
                  Provider.of<VariableWrapper>(context, listen: false)
                      .remove(widget.index);
                  if (Provider.of<VariableWrapper>(context, listen: false)
                          .length() ==
                      0) {
                    // do something on selection reset
                  }
                  setState(() {}); // reloads the thumbnail
                  print(Provider.of<VariableWrapper>(context, listen: false)
                      .getList());
                }
              },
              // child: Image.memory(bytes, fit: BoxFit.cover),
              child: Stack(
                children: [
                  AspectRatio(
                    aspectRatio: 1,
                    child: Image.memory(bytes, fit: BoxFit.cover),
                  ),
                  // Display a Play icon if the asset is a video
                  if (widget.asset.ext?['video'] != null)
                    AspectRatio(
                        aspectRatio: 1,
                        child: Center(
                          child: Container(
                            color: Colors.blue[200],
                            width: 30,
                            height: 30,
                            child: const Icon(
                              Icons.play_arrow,
                              color: Colors.white,
                            ),
                          ),
                        ))
                ],
              ),
            ));
      },
    );
  }
}
