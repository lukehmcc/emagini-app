// package imports
import 'package:flutter/material.dart';

Widget bottomMenu() {
  return Container(
      color: Colors.green,
      child: const TabBar(
        tabs: [
          Tab(text: "Gallery", icon: Icon(Icons.image)),
          Tab(text: "albums", icon: Icon(Icons.inventory)),
        ],
      ));
}
