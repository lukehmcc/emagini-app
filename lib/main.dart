// package imports
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:get/get.dart';

// file imports
import 'wrappers/variable_wrapper.dart';
import 'screens/gallery.dart';
import 'screens/general_settings.dart';
import 'screens/login.dart';

// This is the main script for emagini right now, currently what I'm building is
// two-fold, first theres a basic gallery that I'm populating right now with just
// local files, and at the same time it'll run an async task uploading all of your
// files to skynet and indexing them

// currently only the gallery will work on local files and the indexing and the
// likes will happen only in the background. But with apps like VUP you'll
// be able to see what it's doing

// Also at some point I gotta implement something like a "migrate from google"
// button... maybe I'll make this an akhash thing? I dunno, we'll find out later

void main() {
  // await GetStorage.init();
  runApp(
    ChangeNotifierProvider(
      create: (_) => VariableWrapper(),
      child: const Emagini(),
    ),
  );
}

/// This is the main application widget.
class Emagini extends StatelessWidget {
  const Emagini({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      initialRoute: "/login",
      getPages: [
        GetPage(name: "/", page: () => const StatefullGallery()),
        GetPage(name: "/settings", page: () => const GeneralSettingsScreen()),
        GetPage(name: "/login", page: () => const LoginScreen()),
      ],
    );
  }
}
