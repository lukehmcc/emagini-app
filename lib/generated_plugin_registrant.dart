//
// Generated file. Do not edit.
//

// ignore_for_file: directives_ordering
// ignore_for_file: lines_longer_than_80_chars

import 'package:flutter_secure_storage_web/flutter_secure_storage_web.dart';
import 'package:network_info_plus_web/network_info_plus_web.dart';
import 'package:shared_preferences_web/shared_preferences_web.dart';
import 'package:sodium_libs/src/platforms/sodium_web.dart';
import 'package:video_player_web/video_player_web.dart';
import 'package:wakelock_web/wakelock_web.dart';

import 'package:flutter_web_plugins/flutter_web_plugins.dart';

// ignore: public_member_api_docs
void registerPlugins(Registrar registrar) {
  FlutterSecureStorageWeb.registerWith(registrar);
  NetworkInfoPlusPlugin.registerWith(registrar);
  SharedPreferencesPlugin.registerWith(registrar);
  SodiumWeb.registerWith(registrar);
  VideoPlayerPlugin.registerWith(registrar);
  WakelockWeb.registerWith(registrar);
  registrar.registerMessageHandler();
}
