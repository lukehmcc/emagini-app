// dart imports
import 'dart:io';
import 'dart:typed_data';

// package imports
import 'package:flutter/material.dart';
import 'package:photo_manager/photo_manager.dart';
import 'package:blurhash_dart/blurhash_dart.dart';
import 'package:image/image.dart' as img;
import 'package:filesystem_dac/dac.dart';
import 'package:chunked_stream/chunked_stream.dart';
import 'package:path/path.dart';
import 'package:get/get.dart';
import 'package:skynet/src/client.dart';
import 'package:hive/hive.dart';

// file imports
import '../widgets/gallery_section.dart';
import '../wrappers/asset_object.dart';
import '../modules/date_strings.dart';
import '../screens/video_screen.dart';
import '../screens/image_screen.dart';
import '../widgets/gallery_appbar.dart';
import '../widgets/default_drawer.dart';
import '../widgets/bottom_gallery_menu.dart';
import '../modules/content_indexer.dart';
import '../modules/storage_provider.dart';

// This is the stateful widget that the main application instantiates.
class StatefullGallery extends StatefulWidget {
  const StatefullGallery({Key? key}) : super(key: key);

  @override
  StatefullGalleryState createState() => StatefullGalleryState();
}

/// This is the private State class that goes with MyStatefulWidget.
class StatefullGalleryState extends State<StatefullGallery> {
  // This will hold all the assets we fetched
  List<AssetEntity> assets = [];
  List<List<AssetObject>> sortedAssets = [];
  bool indexComplete = false;
  String uploadingfile = "intializing";
  FileSystemDAC? dac;
  StorageService? storageService;
  late Widget galleryWidget;
  List<String> sortedSkynetAssets = [];
  late SkynetClient skynetClient;
  late String tempDir;
  late Box<dynamic> localFiles;

  @override
  void initState() {
    super.initState();
    _setGallery();
    getLocalIndex().then((value) {
      setState(() {
        sortedSkynetAssets = value;
      });
    });
    _initDAC().then((voidValue) {
      indexFilesAndUploadSkynet(dac!, storageService!);
    });
  }

  // this is passed down the widget tree if the ability to reset global state
  // is requried
  resetState() {
    setState(() {});
  }

  Future<void> _initDAC() async {
    // first gonna intialize the dac
    StorageService localStorageService = StorageService();
    List<dynamic> objectList = await localStorageService.init();
    FileSystemDAC localDac = objectList[0];
    SkynetClient localSkynetClient = objectList[1];
    String localTempDir = objectList[2];
    Box<dynamic> localLocalFiles = objectList[3];
    setState(() {
      dac = localDac;
      storageService = localStorageService;
      tempDir = localTempDir;
      skynetClient = localSkynetClient;
      localFiles = localLocalFiles;
    });
  }

  _setGallery() {
    if (dac != null) {
      setState(() {
        galleryWidget = Scrollbar(
            isAlwaysShown: true,
            controller: _scrollController,
            thickness: 5,
            child: RefreshIndicator(
                onRefresh: () => getLocalIndex().then((value) {
                      setState(() {
                        sortedSkynetAssets = value;
                      });
                    }),
                child: ListView.builder(
                    controller: _scrollController,
                    physics: const AlwaysScrollableScrollPhysics(),
                    cacheExtent: 1000,
                    padding: const EdgeInsets.all(8),
                    itemCount: sortedSkynetAssets.length,
                    itemBuilder: (BuildContext context, int index) {
                      return GallerySection(
                        assetPath: sortedSkynetAssets[index],
                        index: index,
                        dac: dac!,
                        localFiles: localFiles,
                        skynetClient: skynetClient,
                        tempDir: tempDir,
                      );
                    })));
      });
    } else if (sortedSkynetAssets == []) {
      setState(() {
        galleryWidget =
            const Text("There is no local index available... building");
      });
    } else {
      print("dac isn't ready");
      setState(() {
        galleryWidget = const Text("DAC is loading");
      });
    }
  }

  // and build the scroll bar
  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    // now lets build all the rows here
    _setGallery();
    return Scaffold(
      body: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: GalleryAppBar(
            resetState: resetState,
          ),
          drawer: defaultDrawer(),
          bottomNavigationBar: bottomMenu(),
          body: TabBarView(
            children: [
              galleryWidget,
              const Icon(Icons.inventory),
            ],
          ),
        ),
      ),
    );
  }
}
