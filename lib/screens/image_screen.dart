// package imports
import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:get/get.dart';
import 'dart:typed_data';
import 'package:chunked_stream/chunked_stream.dart';
import 'package:blurhash_dart/blurhash_dart.dart';
import 'package:image/image.dart' as image_2;
import 'package:path/path.dart' as pathie;
import 'package:skynet/src/skystandards/fs.dart';
import 'package:filesystem_dac/dac.dart';
import 'package:test/modules/local_files.dart';
import 'package:skynet/src/client.dart';
import 'package:hive/hive.dart';

// file imports
import '../modules/image_caching.dart';
import '../modules/gallery_navigation.dart';
import '../modules/content_indexer.dart';

class ImageScreen extends StatefulWidget {
  const ImageScreen({
    Key? key,
    required this.index,
    required this.bhash,
    required this.dac,
    required this.thumbKey,
    required this.imageFile,
    required this.skynetBaseFilePath,
    required this.skynetClient,
    required this.tempDir,
    required this.localFiles,
  }) : super(key: key);
  final List<int> index;
  final String bhash;
  final FileSystemDAC dac;
  final String thumbKey;
  final DirectoryFile imageFile;
  final String skynetBaseFilePath;
  final SkynetClient skynetClient;
  final String tempDir;
  final Box<dynamic> localFiles;

  @override
  State<ImageScreen> createState() => _ImageScreenState();
}

class _ImageScreenState extends State<ImageScreen> {
  Widget currentPhoto = Container();

  @override
  void initState() {
    super.initState();
    _buildPhoto();
  }

  // build the photo from the stream
  _buildPhoto() async {
    print("building photo");
    if (widget.bhash != "") {
      var blurHash = BlurHash.decode(widget.bhash).toImage(400, 400);
      setState(() {
        currentPhoto = Stack(
          children: [
            Center(
              child:
                  Image.memory(Uint8List.fromList(image_2.encodeJpg(blurHash))),
            ),
            const Center(
              child: CircularProgressIndicator(),
            ),
          ],
        );
      });
    } else {
      setState(() {
        currentPhoto = const CircularProgressIndicator();
      });
    }
    // utilize local file or cache prior to network call
    DirectoryIndex? fileDir =
        widget.dac.getDirectoryIndexCached(widget.skynetBaseFilePath);
    String skynetFilePath = "";
    if (fileDir != null) {
      var filesInDir = fileDir.files.entries.toList();
      skynetFilePath = pathie.join(
        widget.skynetBaseFilePath,
        filesInDir[widget.index[1]].value.name,
      );
      bool isLocal = await checkForLocalImage(skynetFilePath);
      if (isLocal && skynetFilePath != "") {
        getLocalFile(skynetFilePath).then((localFile) {
          setState(() {
            currentPhoto = PhotoView(
              imageProvider: FileImage(localFile!),
            );
          });
        });
        return;
      }
      bool isCached = await checkForCachedImage(skynetFilePath);
      if (isCached && skynetFilePath != "") {
        getCachedImage(skynetFilePath).then((dataBytes) {
          setState(() {
            currentPhoto =
                PhotoView(imageProvider: Image.memory(dataBytes).image);
          });
          return;
        });
      } else {
        // download the photo, set it to the photo state, then cache it
        Future<Stream<Uint8List>> photoFuture = widget.dac
            .downloadAndDecryptFileInChunks(
                filesInDir[widget.index[1]].value.file);
        photoFuture.then((photoData) {
          readByteStream(photoData).then((dataBytes) {
            setState(() {
              currentPhoto =
                  PhotoView(imageProvider: Image.memory(dataBytes).image);
            });
            // now cache it, and the path has to be fetched
            print("caching image");
            cacheImage(dataBytes, skynetFilePath);
          });
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    print("building image view");
    return Listener(
        onPointerMove: (moveEvent) {
          int sensitivity = 10;
          if (moveEvent.delta.dx > sensitivity) {
            changeGalleryLocation(
              widget.index,
              widget.dac,
              false,
              widget.skynetClient,
              widget.tempDir,
              widget.localFiles,
            );
          } else if (moveEvent.delta.dx < -sensitivity) {
            changeGalleryLocation(
              widget.index,
              widget.dac,
              true,
              widget.skynetClient,
              widget.tempDir,
              widget.localFiles,
            );
          } else if (moveEvent.delta.dy > sensitivity) {
            Get.back();
          }
        },
        child: Scaffold(
            extendBodyBehindAppBar: true,
            backgroundColor: Colors.black,
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
              actions: <Widget>[
                Padding(
                    padding: const EdgeInsets.only(right: 20.0),
                    child: GestureDetector(
                      onTap: () {
                        final snackBar = SnackBar(
                          content: const Text('Link Generated'),
                          action: SnackBarAction(
                            label: 'Copy Sharable Link',
                            onPressed: () {
                              // Some code to undo the change.
                            },
                          ),
                        );
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      },
                      child: const Icon(
                        Icons.share,
                        size: 26.0,
                      ),
                    )),
                Padding(
                    padding: const EdgeInsets.only(right: 20.0),
                    child: GestureDetector(
                      onTap: () {
                        const snackBar = SnackBar(
                          content: Text('Deleted'),
                        );
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                        Navigator.pop(context);
                      },
                      child: const Icon(
                        Icons.delete,
                        size: 26.0,
                      ),
                    )),
              ],
            ),
            body: Container(
              color: Colors.black,
              alignment: Alignment.center,
              child: currentPhoto,
            )));
  }
}
