// package imports
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';

// file imports
import '../widgets/default_drawer.dart';
import '../modules/image_caching.dart';

class GeneralSettingsScreen extends StatefulWidget {
  const GeneralSettingsScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<GeneralSettingsScreen> createState() => _GeneralSettingsScreenState();
}

class _GeneralSettingsScreenState extends State<GeneralSettingsScreen> {
  bool tempButtonState = false;
  late TextEditingController _portalConfigController;
  late TextEditingController _jwtConfigController;
  String defaultPortal = "siasky.net"; // hardcoding this here is bad, I know
  String portalJWT = "If this shows something is broken";
  String userPortal = "If this shows something is broken";
  var storage = const FlutterSecureStorage();

  _getPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool? butState = prefs.getBool('tempor');
    bool butStateNonNull = false;
    if (butState == null) {
      await prefs.setBool('tempor', false);
      setState(() {
        butState = false;
      });
    } else {
      butStateNonNull = butState;
    }
    setState(() {
      tempButtonState = butStateNonNull;
    });
  }

  _setPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool('tempor', tempButtonState);
  }

  @override
  void initState() {
    super.initState();
    _portalConfigController = TextEditingController();
    _jwtConfigController = TextEditingController();
    _getPrefs();
  }

  @override
  void dispose() {
    _portalConfigController.dispose();
    _jwtConfigController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    storage.read(key: "user-portal").then((localUserPortal) {
      print("user-portal key is read");
      if (localUserPortal != null) {
        userPortal = localUserPortal;
      } else {
        userPortal = defaultPortal;
      }
    });

    storage.read(key: "portal-jwt").then((localPortalJWT) {
      if (localPortalJWT != null) {
        portalJWT = localPortalJWT;
      } else {
        portalJWT = "";
      }
    });
    return Scaffold(
        appBar: AppBar(),
        drawer: defaultDrawer(),
        body: ListView(
          children: [
            Wrap(
              crossAxisAlignment: WrapCrossAlignment.center,
              children: [
                const Text("arbitrary"),
                Switch(
                    value: tempButtonState,
                    activeColor: Colors.green,
                    onChanged: (bool currState) {
                      setState(() {
                        tempButtonState = currState;
                      });
                      _setPrefs();
                    }),
              ],
            ),
            TextField(
                controller: _portalConfigController,
                // get current portal and populate area with hint
                decoration: InputDecoration(hintText: userPortal),
                onSubmitted: (String phrase) {
                  storage
                      .write(key: "user-portal", value: phrase)
                      .then((value) => null);
                }),
            TextButton(
                onPressed: () {
                  storage
                      .write(
                          key: "user-portal",
                          value: _portalConfigController.text)
                      .then((value) => null);
                },
                child: const Text("Submit")),
            TextField(
                controller: _jwtConfigController,
                // get current portal and populate area with hint
                decoration: InputDecoration(hintText: portalJWT),
                onSubmitted: (String phrase) {
                  storage
                      .write(key: "portal-jwt", value: phrase)
                      .then((value) => null);
                }),
            TextButton(
                onPressed: () {
                  storage
                      .write(
                          key: "portal-jwt", value: _jwtConfigController.text)
                      .then((value) => null);
                },
                child: const Text("Submit")),
            TextButton(
                onPressed: () {
                  storage.write(key: "user-seed", value: "");
                  Get.offAllNamed("/login");
                },
                child: const Text("Log out")),
            TextButton(
                onPressed: () {
                  // clear cache and show snackbar to notify the user
                  clearCache();
                  const snackBar = SnackBar(
                    content: Text('Cache Cleared'),
                  );
                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                },
                child: const Text("Clear Local Cache")),
          ],
        ));
  }
}
