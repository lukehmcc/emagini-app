// package imports
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:chewie/chewie.dart';
import 'package:test/modules/gallery_navigation.dart';
import 'package:test/modules/temporary_streaming_service.dart';
import 'package:video_player/video_player.dart';
import 'package:skynet/src/skystandards/fs.dart';
import 'package:filesystem_dac/dac.dart';
import 'package:skynet/src/client.dart';
import 'package:hive/hive.dart';
import 'package:get/get.dart';
import 'package:blurhash_dart/blurhash_dart.dart';
import 'dart:typed_data';
import 'package:image/image.dart' as image_2;
// file imports

class VideoScreen extends StatefulWidget {
  const VideoScreen({
    Key? key,
    required this.videoFile,
    required this.index,
    required this.skynetClient,
    required this.dac,
    required this.tempDir,
    required this.localFiles,
    required this.skynetBaseFilePath,
    required this.bhash,
  }) : super(key: key);
  // I'm aware that use a nullable type here is kinda bad practice, but I check
  // for the null before I pass the file to this function so there *isn't really*
  // any issue with this
  final DirectoryFile videoFile;
  final List<int> index;
  final String skynetBaseFilePath;
  final FileSystemDAC dac;
  final SkynetClient skynetClient;
  final String tempDir;
  final Box<dynamic> localFiles;
  final String bhash;

  @override
  _VideoScreenState createState() => _VideoScreenState();
}

class _VideoScreenState extends State<VideoScreen> {
  TargetPlatform? _platform;
  late VideoPlayerController _controller;
  ChewieController? _chewieController;
  bool initialized = false;
  late TemporaryStreamingServerService service;
  Widget currentVideo = const CircularProgressIndicator();

  @override
  void initState() {
    _initVideo();
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    _chewieController?.dispose();
    service.stop();
    super.dispose();
  }

  _initVideo() async {
    if (widget.bhash != "") {
      var blurHash = BlurHash.decode(widget.bhash).toImage(400, 400);
      setState(() {
        currentVideo = Stack(
          children: [
            Center(
              child:
                  Image.memory(Uint8List.fromList(image_2.encodeJpg(blurHash))),
            ),
            const Center(
              child: CircularProgressIndicator(),
            ),
          ],
        );
      });
    }
    service = TemporaryStreamingServerService();
    String videoURL = await service.makeFileAvailable(widget.videoFile,
        widget.skynetClient, widget.dac, widget.tempDir, widget.localFiles);
    print("VIDEO URL: " + videoURL);
    _controller = VideoPlayerController.network(videoURL);
    // _controller = VideoPlayerController.file(video!);

    await _controller.initialize();

    setState(() {
      _controller.pause();
      _controller.seekTo(const Duration());
      _createChewieController();
    });

    setState(() => initialized = true);
  }

  void _createChewieController() {
    _chewieController = ChewieController(
      videoPlayerController: _controller,
      autoPlay: true,
      looping: true,
      optionsTranslation: OptionsTranslation(
        playbackSpeedButtonText: 'Playback Speed',
        cancelButtonText: 'Cancel',
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Listener(
        onPointerMove: (moveEvent) {
          int sensitivity = 10;
          if (moveEvent.delta.dx > sensitivity) {
            changeGalleryLocation(
              widget.index,
              widget.dac,
              false,
              widget.skynetClient,
              widget.tempDir,
              widget.localFiles,
            );
          } else if (moveEvent.delta.dx < -sensitivity) {
            changeGalleryLocation(
              widget.index,
              widget.dac,
              true,
              widget.skynetClient,
              widget.tempDir,
              widget.localFiles,
            );
          } else if (moveEvent.delta.dy > sensitivity) {
            Get.back;
          }
        },
        // child: const CircularProgressIndicator());
        child: Scaffold(
          // I'd like to make it extend at some point, but doing this breaks the
          // scrubber currently and I can't find an easy fix
          // extendBodyBehindAppBar: true,
          backgroundColor: Colors.black,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            actions: <Widget>[
              Padding(
                  padding: const EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      final snackBar = SnackBar(
                        content: const Text('Link Generated'),
                        action: SnackBarAction(
                          label: 'Copy Sharable Link',
                          onPressed: () {
                            // Some code to undo the change.
                          },
                        ),
                      );
                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                    },
                    child: const Icon(
                      Icons.share,
                      size: 26.0,
                    ),
                  )),
              Padding(
                  padding: const EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      const snackBar = SnackBar(
                        content: Text('Deleted'),
                      );
                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      Navigator.pop(context);
                    },
                    child: const Icon(
                      Icons.delete,
                      size: 26.0,
                    ),
                  )),
            ],
          ),
          body: initialized
              // If the video is initialized, display it
              ? Scaffold(
                  body: Column(
                    children: <Widget>[
                      Expanded(
                        child: Center(
                          child: _chewieController != null &&
                                  _chewieController!
                                      .videoPlayerController.value.isInitialized
                              ? Chewie(
                                  controller: _chewieController!,
                                )
                              : Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: const [
                                    CircularProgressIndicator(),
                                    SizedBox(height: 20),
                                    Text('Loading'),
                                  ],
                                ),
                        ),
                      ),
                    ],
                  ),
                )
              : currentVideo,
        ));
  }
}
