// package imports
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:skynet/src/mysky_seed/derivation.dart';
import 'package:get/get.dart';

// file imports
import '../screens/gallery.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late TextEditingController _controller;

  @override
  void initState() {
    _checkForSeedAndMakeSureItsCorrect().then((isThereSeed) {
      if (isThereSeed) {
        Get.off(const StatefullGallery());
      }
    });
    super.initState();
    _controller = TextEditingController();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Future<bool> _checkForSeedAndMakeSureItsCorrect() async {
    var storage = const FlutterSecureStorage();
    String? value = await storage.read(key: "user-seed");
    if (value != null) {
      try {
        validatePhrase(value);
      } catch (e) {
        return false;
      }
      return true;
    } else {
      return false;
    }
  }

  Future<void> seedPopup(BuildContext contextOther, String phrase) {
    return showDialog<void>(
        context: contextOther,
        builder: (BuildContext context) {
          // test if the seed is valid
          try {
            validatePhrase(phrase);
          } catch (e) {
            // if it isn't pop up an error and make them try again
            return AlertDialog(
              title: const Text('Seed is not valid'),
              content: Text(e.toString()),
              actions: <Widget>[
                TextButton(
                  onPressed: () {
                    Get.back();
                  },
                  child: const Text('Try again'),
                ),
              ],
            );
          }

          // save seed
          var storage = const FlutterSecureStorage();
          storage.write(key: "user-seed", value: phrase);

          //  go to gallery
          return AlertDialog(
            title: const Text('Seed has been input!'),
            content: const Text('Your seed has now been saved securely.'),
            actions: <Widget>[
              TextButton(
                onPressed: () {
                  Get.offAll(const StatefullGallery());
                },
                child: const Text('ok'),
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(),
        body: Column(
          children: [
            TextField(
                controller: _controller,
                decoration: const InputDecoration(hintText: "Input user seed"),
                onSubmitted: (String phrase) {
                  seedPopup(context, phrase);
                }),
            TextButton(
                onPressed: () {
                  seedPopup(context, _controller.text);
                },
                child: const Text("Submit")),
          ],
        ));
  }
}
