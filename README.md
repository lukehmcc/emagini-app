# emagini

A photo storage app build in flutter and on top of Redsolvers framework that he built VUP with.

## I'm working on it 

TODO for goodness: 
* ~~Rudimentary gallery app that just shows photos~~
* ~~Make clicking on photos push the user to a individualized view of each photo/video~~
* ~~Make the photo viewer better~~
* ~~Make the video viewer better(use chewie for that material design goodness)~~
* ~~Add overlays for back, sharing, and deletion when inside on of the individualized views(share and delete don't have to work properly yet)~~
* ~~Make the gallery viewer more responsive(so make it scale the amount of photos on screen in accordance to screen size)~~
* ~~Add basic mass selection for the main gallery view~~
* ~~Add delete overlay for selection in gallery view~~
* ~~Add "swipe to go to next item in gallery" function~~
* ~~Fix broken selection~~
* ~~Create basic file indexing to make stuff like sorting (and navigating by date) possible~~
* ~~Sort photos by date and section them off daily based on the last point~~
* Add album tab and integrate making albums into the mass selection thing
* ~~Integrate blurhash and loading circle into photo and video viewer while we're waiting for it to load~~
* ~~Add super basic settings panel~~
* ~~Video support!~~
  - ~~Thumbnail generation and gallery support~~
  - ~~Video streaming~~
* Dark theme
* Information panel for each photo 
* Information panel for albums
* ~~ Integrate SkyFS and continuious upload code(steal it from VUP(may have to do it without stealing code becuase it isn't open source yet))~~
* ~~ Refactor the gallery code to only display from the SkyFS ~~
* ~~Add local caching of photos for offline use / faster traversing~~
* ~~ Figure out how to get the SkyFS query-able from offline(this shouldn't be necesary because SkyFS caches for offline use) ~~
* Make share and delete buttons work properly in tandem with the SkyFS and locally on the device 
  - Share
  - Delete
* Add a "clear out xGB of phone space" button in the settings
* Portal switching
* Finally, kill bill

TODO when there is time to kill:
* Migration from google photos
* Migration from apple photos 
* Search...
* Facial recog locally? Dunno if this is even practical
* Set up integration with "1st party" emagini portal and set it up with google play and apple pay so it's super easy for new users