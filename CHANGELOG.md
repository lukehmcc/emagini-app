# The best place to find out what I'm up to!

## 0.3.1 
* Fixed gallery bug where only cached thumbnails would load

## 0.3.0
* Added support for streaming of videos!
* Made the gallery a scale a bit better

## 0.2.0 
* Added support for thumbnail generation and gallery view of videos

## 0.1.0
* Added support for viewing local files as opposed to always fetching uploaded files from SkyFS
* Minor bug fixes on app init